<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Mails 
{
    use HasFactory;

    public static function all(){
        return DB::select(" SELECT * FROM mailsList ");
    }

    public static function create($email, $timer){
        DB::insert(" INSERT INTO mailsList (email, timer) VALUES (:email, :timer) ", [
            "email"=>$email,
            "timer"=>$timer
        ]);
    }

    public static function update($id, $email){
        DB::table('mailsList')
                ->where('id', $id)
                ->update(['email' => $email]);
    }

    public static function delete($id){
        DB::table('mailsList')
                ->where('id', $id)
                ->delete();
    }
}
