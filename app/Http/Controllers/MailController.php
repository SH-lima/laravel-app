<?php

namespace App\Http\Controllers;

use App\Models\Mails;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;



class MailController extends Controller
{
    public function sendMail($email){
        Mail::send('mail',['username' =>'test', 'date'=> date("d-m-Y")], function($message) use ($email){

            $message->to($email)->subject('Welcome');
        });
    }

    public function readALL(){
    
        $arr_data = Mails::all();
        return $arr_data;
    
    }
    public function addMail(Request $request){
    
        $validated=$request->validate([
            "email"=>"required",
            "timer"=>"required"
        ]);
        $email=$validated["email"];
        $timer=$validated["timer"];
        // // model
        Mails::create($email, $timer);
        return redirect('/');
        exit;
    
    }

    public function changeMail(Request $request){
    
        $validated=$request->validate([
            "email"=>"required",
            "id"=>"required"
        ]);
        $email=$validated["email"];
        $id=$validated["id"];
        // // model
        Mails::update($id, $email);
        return redirect('/');
        exit;
    
    }

    public function deleteMail(Request $request){
        $validated=$request->validate([
            "id"=>"required"
        ]);
        $id=$validated["id"];
        Mails::delete($id);
        return redirect('/');
        exit;
        
    
    }


} 

