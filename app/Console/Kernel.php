<?php

namespace App\Console;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\MailController;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        function intervalle($timer, $num){
            if($timer == $num){
                return true;
            }
        }

        $data = DB::select(" SELECT * FROM mailsList ");
        if(count($data)>0){}
            foreach($data as $element){
                $data_mail = $element->email;
                $data_timer = $element->timer;
                


                $schedule->call(function() use($data_mail){
                    MailController::sendMail($data_mail); 
                })
                ->everyFiveMinutes()
                ->when(intervalle($data_timer, 5));
            
                $schedule->call(function() use($data_mail){
                    MailController::sendMail($data_mail); 
                })
                ->everyMinute()
                ->when(intervalle($data_timer, 1));
                
                $schedule->call(function() use($data_mail){
                    MailController::sendMail($data_mail); 
                })
                ->everyTenMinutes()
                ->when(intervalle($data_timer, 10));
            
            
            

            }
            
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

