<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MailController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('mail', [MailController::class,'sendMail']);

Route::get('mails', [MailController::class,'readAll']);

Route::post('addmail', [MailController::class,'addMail']);

Route::post('changemail', [MailController::class,'changeMail']);

Route::post('delete', [MailController::class,'deleteMail']);